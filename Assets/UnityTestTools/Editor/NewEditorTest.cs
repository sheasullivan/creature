﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class NewEditorTest {

	[Test]
	public void EditorTest()
	{
		//Arrange
		var gameObject = new GameObject();

		//Act
		//Try to rename the GameObject
        var c = gameObject.GetComponent<Animator>();

        //Assert
        //The object has a new name
        Assert.IsTrue(c != null);
        Assert.AreEqual(c, null);
	}
}
