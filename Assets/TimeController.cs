﻿using UnityEngine;
using System.Collections;

public class TimeController : MonoBehaviour {

	void Update () {
        //Time.timeScale = Mathf.Clamp(Input.GetAxis("Time"), 0, 10);
        if (Input.GetKeyDown(KeyCode.Comma))
        {
            Time.timeScale = Mathf.Clamp(Time.timeScale - 1, 1, 10);
        }

        if (Input.GetKeyDown(KeyCode.Period))
        {
            Time.timeScale = Mathf.Clamp(Time.timeScale + 1, 1, 10);
        }
    }
}
