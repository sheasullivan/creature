﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Creature.NeuralNetwork;

public class Health : MonoBehaviour {
	public double HP = 100;
    public double AgingRate = 1;

	// Use this for initialization
	void Start () {
		StartCoroutine (Tick ());
	}
	
	// Update is called once per frame
	void Update () {
		foreach (var segment in GetComponent<CreatureController>().bodySegments) {
			var r = segment.GetComponent<Renderer>();
			float red = ((100f - (float)HP) / 100f);
			float green = ((float)HP / 100f);
			float blue = 0;//1 - red - green;
			r.material.color = new Color(red, green, blue);
		}

		if (HP <= 0) {
			Die();
		}
	}

	public void Eat(Food food) {
		HP += food.HPBonus;
	}

	private IEnumerator Tick() {
		//var neurons = GetComponent<NervousSystem> ().Brain.CellsOfType<INeuron>();
		//var dendritesCount = neurons.Select (neuron => neuron.dendrites.Count).Sum ();
		//var segmentsCount = GetComponentsInChildren<BodySegment>().Length;
		while (true) {
			yield return new WaitForSeconds(1f);
			//HP -= 1 + 0.5 * Mathf.Log(segmentsCount);
			//HP -= Mathf.Pow((float)dendritesCount / 100f, 2f);
			HP -= AgingRate;
		}
	}

	private void Die() {
		//GameObject.Find ("Spawn").GetComponent<Spawn> ().Population.Death (gameObject);
		foreach (var segment in GetComponent<CreatureController>().bodySegments) {
			Object.Destroy(segment.gameObject);
		}
		Object.Destroy (gameObject);
	}
}
