﻿using UnityEngine;
using System.Collections;
using Creature.NeuralNetwork;
using System.Collections.Generic;

public class Nose : MonoBehaviour {
	public IDictionary<Smell, string> sensorCells = new Dictionary<Smell, string>();
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/*void OnTriggerStay(Collider other) {
		Debug.Log (other);
		var scent = other.GetComponent<Scent> ();
		if (scent != null) {
			Sense(scent);
		}
	}*/

	public void Sniff() {
		var colliders = Physics.OverlapSphere (transform.root.GetChild(0).transform.position, 10, LayerMask.GetMask(new string[] { "Food" }));
		var smellIntensities = new Dictionary<Smell, double>();
		foreach (var collider in colliders) {
			foreach (var scent in collider.GetComponents<Scent>()) {
				Sense(scent, smellIntensities);
			}
		}
	}
	
	public void Sense(Scent scent, IDictionary<Smell, double> smellIntensities) {
		var nervousSystem = GetComponent<NervousSystem> ();
		if (nervousSystem == null || nervousSystem.Brain == null) {
			return;
		}
		if (sensorCells != null && sensorCells.ContainsKey (scent.smell)) {
			if (smellIntensities.ContainsKey(scent.smell)) {
				smellIntensities[scent.smell] += scent.intensity;
			} else {
				smellIntensities[scent.smell] = scent.intensity;
			}
			var sensorGuid = sensorCells [scent.smell];
			if (!System.String.IsNullOrEmpty(sensorGuid)) {
				((SensorCell)nervousSystem.Brain.GetCellByGuid (sensorGuid)).input = smellIntensities[scent.smell];
			}
		}
	}

	/*public void Reset() {
		smellIntensities = new Dictionary<Smell, double> ();

		var nervousSystem = transform.root.GetComponent<NervousSystem> ();
		if (nervousSystem == null || nervousSystem.Brain == null) {
			return;
		}
		foreach (var kvp in sensorCells) {
			var sensorGuid = kvp.Value;
			((SensorCell)nervousSystem.Brain.GetCellByGuid (sensorGuid)).input = 0;
		}
	}*/

	public void ConnectNervousSystem() {
		sensorCells = new Dictionary<Smell, string> ();
		var nervousSystem = GetComponent<NervousSystem> ();
		if (nervousSystem == null || nervousSystem.Brain == null) {
			return;
		}
		var activationFunction = nervousSystem.ActivationFunction;	
		var scf = new SensorCell (activationFunction);
		sensorCells.Add(Smell.Food, scf.guid);
		nervousSystem.Brain.AddNewCell(scf, nervousSystem.sensorLayer);
		var scp = new SensorCell (activationFunction);
		sensorCells.Add(Smell.Poison, scp.guid);
		nervousSystem.Brain.AddNewCell(scp, nervousSystem.sensorLayer);
	}
}
