﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Creature.NeuralNetwork {

	public class Brain {
		//public IList<ICell> cells;
        private IList<INetworkLayer> layers;

		public Brain() {
			//cells = new List<ICell> ();
            layers = new List<INetworkLayer>();
		}

        //public IList<T> CellsOfType<T>() {
        //	return cells
        //		.Where(x => typeof(T).IsAssignableFrom(x.GetType()))
        //			.Select(x => (T)x)
        //			.ToList();
        //}

        public IList<ICell> GetCells()
        {
            return layers.SelectMany(layer => layer.cells).ToList();
        }

        public IList<ICell> GetCells(int layer)
        {
            return layers[layer].cells;
        }

		public void Evaluate() {
            foreach (NetworkLayer layer in layers)
            {
                foreach (ICell cell in layer.cells)
                {
                    cell.Evaluate();
                }
            }
		}

        public void AddLayer()
        {
            layers.Add(new NetworkLayer());
        }

		public void AddNewCell (SensorCell cell, int layer)
		{
			ConnectToNewCell (cell, layers.ElementAtOrDefault(layer + 1));
            layers[layer].cells.Add(cell);
			//cells.Add (cell);
		}

		public void AddNewCell (INeuron cell, int layer)
		{
            ConnectToNewCell(cell, layers.ElementAtOrDefault(layer + 1));
            ConnectNewNeuronToOthers (cell, layers.ElementAtOrDefault(layer - 1));
            layers[layer].cells.Add(cell);
            //cells.Add (cell);
        }

		public void Randomize() {
            foreach (NetworkLayer layer in layers) {
                foreach (var neuron in layer.cells.OfType<INeuron>()) {
                    foreach (var d in neuron.dendrites) {
                        d.Strength = Random.Range(-1f, 1f);
                    }
                }
            }
		}

		//public ICell GetRandomCell() {
		//	return cells.OrderBy(x => System.Guid.NewGuid()).First();
		//}

		public ICell GetCellByGuid(string guid) {
			if (System.String.IsNullOrEmpty (guid)) {
				return null;
			}
			return layers.SelectMany(layer => layer.cells.Where (cell => cell.guid == guid)).FirstOrDefault ();
		}

		public Brain Clone() {
			var clone = new Brain ();
            clone.layers = layers.Select(layer =>  layer.ShallowClone()).ToList();
            foreach (var layer in layers)
            {
                layer.CloneConnections(clone);
            }
            //clone.cells = cells.Select (cell => cell.ShallowClone()).ToList();

            //foreach (var layer in layers)
            //{
            //    foreach (var cell in layer.cells.OfType<INeuron>())
            //    {
            //        cell.CloneDendrites(clone);
            //    }
            //}
			return clone;
		}

		private void ConnectToNewCell(ICell cell, INetworkLayer nextLayer) {
            if (nextLayer == null) { return; }

			foreach (var neuron in nextLayer.cells.OfType<INeuron>()) {
				neuron.dendrites.Add(new Dendrite(cell, Random.Range(-1f, 1f)));
			}
		}

		private void ConnectNewNeuronToOthers(INeuron neuron, INetworkLayer previousLayer) {
            if (previousLayer == null) { return; }

			foreach (var other in previousLayer.cells.OfType<INeuron>()) {
				neuron.dendrites.Add(new Dendrite(other, Random.Range(-1f, 1f)));
			}
		}


	}

    public interface ICloneable
    {
        ICloneable CloneSelf(ICloneable parent);
        ICloneable CloneChildren();
    }

    public interface INetworkLayer
    {
        IList<ICell> cells { get; set; }
        //INetworkLayer nextLayer { get; set; }
        //INetworkLayer previousLayer { get; set; }

        void AddCell(ICell cell);
        INetworkLayer ShallowClone();
        void CloneConnections(Brain newBrain);
    }

    public class NetworkLayer : INetworkLayer
    {
        public IList<ICell> cells { get; set; }
        //public INetworkLayer nextLayer { get; set; }
        //public INetworkLayer previousLayer { get; set; }

        public NetworkLayer()
        {
            cells = new List<ICell>();
        }

        public void AddCell(ICell cell)
        {
            cells.Add(cell);
        }

        public INetworkLayer ShallowClone()
        {
            var clone = new NetworkLayer();
            clone.cells = cells.Select(cell => cell.ShallowClone()).ToList();
            return clone;
        }

        public void CloneConnections(Brain newBrain)
        {
            foreach (var cell in cells) {
                cell.CloneConnections(newBrain.GetCellByGuid(cell.guid), newBrain);
            }
        }
    }

    public interface ICell {
		string guid { get; }
		double lastOutput { get; set; }
		double Evaluate ();
		ICell ShallowClone ();
        void CloneConnections(ICell newCell, Brain newBrain);
	}

	public interface INeuron : ICell {
		IList<Dendrite> dendrites { get; set; }
		IActivationFunction activationFunction { get; set; }
	}

	public class Neuron : INeuron {
		public IList<Dendrite> dendrites { get; set; }
		public double lastOutput { get; set; }
		public IActivationFunction activationFunction { get; set; }
		public string guid { get; private set; }
		
		
		public Neuron(IActivationFunction activationFunction) {
			guid = System.Guid.NewGuid ().ToString();
			lastOutput = 0;
			this.activationFunction = activationFunction;
			dendrites = new List<Dendrite> ();
		}
		
		public double Evaluate() {
			if (dendrites == null || dendrites.Count == 0) {
				lastOutput = 0;
			} else {
				lastOutput = 0;
                foreach (var d in dendrites)
                {
                    lastOutput += d.Evaluate();
                }
                lastOutput = activationFunction.Evaluate(lastOutput);
                //lastOutput = activationFunction.Evaluate(dendrites.Select (x => x.Evaluate ()).Sum ());
            }
            //lastOutput = activationFunction.Evaluate(dendrites.Aggregate (0.0, (acc, dendrite) =>
            //                                  acc + dendrite.Evaluate ()));
            return lastOutput;
		}

        public ICell ShallowClone()
        {
            var clone = this.MemberwiseClone() as Neuron;
            clone.activationFunction = activationFunction.Clone();
            return clone;
        }

		public void CloneConnections(ICell newCell, Brain newBrain) {
            var newNeuron = newCell as INeuron;
			newNeuron.dendrites = dendrites.Select (dendrite => dendrite.Clone (newBrain)).ToList();
		}
	}

	/*public class MotorCell : INeuron {
		public double lastOutput { get; set; }
		public IList<Dendrite> dendrites { get; set; }
		public IActivationFunction activationFunction { get; set; }
		public System.Guid guid { get; private set; }
		//public BodySegment bodySegment;
		//public Vector3 motorAxis;
		
		public MotorCell(IActivationFunction activationFunction) {
			this.activationFunction = activationFunction;
			//this.bodySegment = bodySegment;
			//this.motorAxis = motorAxis;
			lastOutput = 0;
			dendrites = new List<Dendrite> ();
		}
		
		public double Evaluate() {
			lastOutput = activationFunction.Evaluate(dendrites.Aggregate (0.0, (acc, dendrite) =>
			                                                              acc + dendrite.Evaluate ()));
			return lastOutput;
		}

		public void Go() {
			var delta = motorAxis * lastOutput;
			bodySegment.jointMovementInput += delta;
		}
		
		public MotorCell ShallowClone() {
			var clone = this.MemberwiseClone () as MotorCell;
			clone.activationFunction = activationFunction.Clone ();
		}
		
		public void CloneDendrites(Brain newBrain) {
			dendrites = dendrites.Select (dendrite => dendrite.Clone (newBrain));
		}
	}*/

	public class SensorCell : ICell {
		public double lastOutput { get; set; }
		public IActivationFunction activationFunction { get; set; }
		public string guid { get; private set; }
		public double input = 0;

		public SensorCell(IActivationFunction activationFunction) {
			guid = System.Guid.NewGuid ().ToString();
			this.activationFunction = activationFunction;
			lastOutput = 0;
		}
		
		public double Evaluate() {
			lastOutput = activationFunction.Evaluate(input);
			return lastOutput;
		}

		public ICell ShallowClone() {
			var clone = this.MemberwiseClone () as SensorCell;
			clone.activationFunction = activationFunction.Clone ();
			return clone;
		}

        public void CloneConnections(ICell newCell, Brain newBrain)
        {
            //No connections to clone
        }
    }
	

	
	public class Dendrite {
		public string guid { get; private set; }
		public ICell inputCell;
		public double Strength = 0;
		
		public Dendrite(ICell inputCell) {
            if (inputCell == null) { throw new System.ArgumentNullException("inputCell"); }

			guid = System.Guid.NewGuid ().ToString();
			this.inputCell = inputCell;
		}

		public Dendrite(ICell inputCell, double strength) {
            if (inputCell == null) { throw new System.ArgumentNullException("inputCell"); }

            guid = System.Guid.NewGuid ().ToString();
			this.inputCell = inputCell;
			Strength = strength;
		}

		public double Evaluate() {
			if (inputCell == null) {
				return 0;
			}
			return inputCell.lastOutput * Strength;
		}

		public Dendrite Clone (Brain newBrain)
		{
            var clone = this.MemberwiseClone() as Dendrite;
            clone.inputCell = newBrain.GetCellByGuid(inputCell.guid);
            return clone;
		}
	}
	
	public interface IActivationFunction {
		double Evaluate(double x);
		IActivationFunction Clone ();
	}
	
	public class SigmoidFunction : IActivationFunction {
		public double Alpha = 2;
		
		public SigmoidFunction() {
		}
		
		public double Evaluate(double x) {
			return (1 / (1 + System.Math.Exp (-Alpha * x)));
		}

		public IActivationFunction Clone() {
			return this.MemberwiseClone () as SigmoidFunction;
		}
	}

}
