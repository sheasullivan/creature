﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Creature.Genetics;

public class Population {
	public int MaxPopulation = 20;
	
	public IDictionary<GameObject, Genome> Individuals;
	public CreatureFactory CreatureFactory;
	
	public Population(CreatureFactory creatureFactory) {
		Individuals = new Dictionary<GameObject, Genome> ();
		CreatureFactory = creatureFactory;
	}
	
	public GameObject Birth(Genome genome) {
		if (Individuals.Count >= MaxPopulation) {
			return null;
		}
		
		var gameCreature =  CreatureFactory.Create (genome, Random.onUnitSphere * 10);
		Individuals.Add(gameCreature, genome);
		return gameCreature;
	}
	
	public void Death(GameObject creature) {
		Individuals.Remove (creature);
		Object.Destroy (creature);
	}
}
*/