﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Creature.Genetics.Brain {
	public struct Neuron {
		public readonly System.Guid ID;
		public readonly IDictionary<System.Guid, double> ConnectedNeurons;

		public Neuron (IDictionary<System.Guid, double> connectedNeurons) {
			ID = System.Guid.NewGuid ();
			ConnectedNeurons = connectedNeurons;
		}

		public Neuron (System.Guid id, IDictionary<System.Guid, double> connectedNeurons) {
			ID = id;
			ConnectedNeurons = connectedNeurons;
		}
	}

	public class BrainGenome {
		public IList<Neuron> Neurons;
		
		public BrainGenome() {
			Neurons = new List<Neuron> ();
		}
		
		public Neuron GetNeuronByID(System.Guid id) {
			return Neurons.Where (neuron => neuron.ID == id).First ();
		}
		
		public BrainGenome Clone() {
			var clone = new BrainGenome ();
			clone.Neurons = new List<Neuron> (Neurons);
			return clone;
		}
	}

	public class BrainMutation {
		public double ConnectionAddProbability = 0.1;
		public double ConnectionRemoveProbability = 0.1;
		public double SproutProbability = 0.1;
		public double PruneProbability = 0.1;
		public double MutateConnectionWeightProbability = 0.3;
		
		public BrainGenome Mutate(BrainGenome genome) {
			var newGenome = genome.Clone ();
			newGenome.Neurons = genome.Neurons.Select (neuron => Mutate (genome, neuron)).ToList();
			
			var toAdd = new List<Neuron> ();
			foreach (var neuron in newGenome.Neurons) {
				if (Random.Range (0f, 1f) < SproutProbability) {
					var newConnections = new Dictionary<System.Guid, double>();
					newConnections.Add(GetRandomNeuronID(genome), GetRandomWeight());
					newConnections.Add(GetRandomNeuronID(genome), GetRandomWeight());
					toAdd.Add(new Neuron(newConnections));
				}
			}
			foreach (var neuron in toAdd) {
				newGenome.Neurons.Add(neuron);
			}
			
			var toPrune = new List<Neuron> ();
			foreach (var neuron in newGenome.Neurons) {
				if (Random.Range (0f, 1f) < PruneProbability) {
						toPrune.Add(neuron);
				}
			}
			foreach (var neuron in toPrune) {
				newGenome.Neurons.Remove(neuron);
			}
			return newGenome;
		}

		public Neuron Mutate(BrainGenome genome, Neuron neuron) {
			var newConnections = neuron.ConnectedNeurons;
			if (Random.Range (0f, 1f) < ConnectionRemoveProbability) {
				newConnections.Remove(newConnections.Keys.OrderBy(x => System.Guid.NewGuid()).First());
			}
			if (Random.Range (0f, 1f) < ConnectionAddProbability) {
				newConnections.Add(GetRandomNeuronID(genome), GetRandomWeight());
			}

			newConnections = newConnections.Select (kvp => {
				var value = kvp.Value;
				if (Random.Range (0f, 1f) < MutateConnectionWeightProbability) {
					value = GetRandomWeight();
				}
				return new KeyValuePair<System.Guid, double>(kvp.Key, value);
			}).ToDictionary(pair => pair.Key, pair => pair.Value);;

			return new Neuron (neuron.ID, newConnections);
		}
		
		private System.Guid GetRandomNeuronID(BrainGenome genome) {
			return genome.Neurons.OrderBy(x => System.Guid.NewGuid()).First().ID;
		}

		private double GetRandomWeight() {
			return Random.Range(-1f, 1f);
		}
	}
}
*/