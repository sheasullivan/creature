﻿using UnityEngine;
using System.Collections;
using Creature.Genetics;


public class Reproduction : MonoBehaviour {
	public double ReproductionProbability = 0.01;
	public double HealthReproductionBonus = 5;
	public double SpawnRadius = 5;
	public int PopulationLimit = 25;
	public Mutation Mutation = new Mutation();

	// Use this for initialization
	void Start () {
		StartCoroutine (Tick ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private IEnumerator Tick() {
		while (gameObject.activeInHierarchy) {
			yield return new WaitForSeconds(1f);
			var prob = ReproductionProbability;
			if (GetComponent<Health>().HP > 100) {
				prob *= HealthReproductionBonus;
			}
            var segmentsCount = GetComponentsInChildren<BodySegment>().Length;
            var probabilityEvent = MyUtilities.RollProbability(prob);
            var creaturesCount = GameObject.FindGameObjectsWithTag("Creature").Length;

            if (probabilityEvent && creaturesCount < PopulationLimit && segmentsCount >= 1 ) {
				var birthOffset = Random.onUnitSphere * (float)SpawnRadius;
                birthOffset.y = 0;

				var offspring = Instantiate(gameObject, transform.position + birthOffset + Vector3.up * 3, Quaternion.identity) as GameObject;
				offspring.name = "Creature " + System.Guid.NewGuid();
				offspring.GetComponent<Health>().HP = 100;

                var nervousSystem = GetComponent<NervousSystem>();
                var offspringNervousSystem = offspring.GetComponent<NervousSystem>();
                offspringNervousSystem.Brain = nervousSystem.Brain.Clone();
				Mutation.Mutate(offspring);
                offspring.GetComponent<NervousSystem>().FindNeurons();

                /*var population = GameObject.Find ("Spawn").GetComponent<Spawn> ().Population;
				var genome = population.Individuals[gameObject];
				var offspringGenome = Mutation.Mutate(genome);
				population.Birth(offspringGenome);*/
                Resources.UnloadUnusedAssets();
            }
		}
	}
}
