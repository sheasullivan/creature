﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Creature.NeuralNetwork;
using Creature.Genetics;

public class Spawn : MonoBehaviour {
	public GameObject BaseCreature;
	//public GameObject Segment;
	//public Population Population;
	public double Radius = 20;
	public int InitialPopulation = 1;

	// Use this for initialization
	void Start () {
		//var creatureFactory = new CreatureFactory (transform, BaseCreature, Segment);
		//Population = new Population (creatureFactory);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.FindWithTag("Creature") == null) {
			SpawnInitialPopulation ();
		}
	}
	
	public void SpawnInitialPopulation() {
		/*var genome = new Genome ();
		genome.BodyGenome.Segments.Add (new BodySegment (Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [0], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [0], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [1], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [2], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));

		var mutation = new Mutation ();
		for (int x = 0; x < 10; x++) {
			var mutatedGenome = mutation.Mutate(genome);
			Population.Birth(mutatedGenome);
		}*/
		for (int i = 0; i < InitialPopulation; i++) {
			var c = Random.insideUnitCircle;
			var position = transform.position + new Vector3(c.x, 0, c.y) * (float)Radius;
			var creature = Instantiate(BaseCreature);
            creature.name = "Creature " + System.Guid.NewGuid();
            creature.transform.position = position;
			creature.GetComponent<NervousSystem>().Setup();
            foreach (var segment in creature.GetComponentsInChildren<BodySegment>())
            {
                segment.Setup();
            }
            foreach (var nose in creature.GetComponentsInChildren<Nose>()) {
				nose.ConnectNervousSystem();
			}



			//var s0 = creature;
   //         s0.GetComponent<BodySegment>().creature = creature;
   //         var s1 = s0.GetComponent<BodySegment>().SproutNewSegment();
			//var s2 = s1.GetComponent<BodySegment>().SproutNewSegment();
			//var s3 = s2.GetComponent<BodySegment>().SproutNewSegment();
			//var s4 = s0.GetComponent<BodySegment>().SproutNewSegment();
			//s1.GetComponent<BodySegment>().SproutNewSegment();
			//s2.GetComponent<BodySegment>().SproutNewSegment();
		}

	}
}

