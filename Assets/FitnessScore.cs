﻿using UnityEngine;
using System.Collections;
using System;

public class FitnessScore {
    public GameObject gameObject { get; set; }
    public float score { get
        {
            var delta = startPosition - gameObject.transform.position;
            return delta.magnitude;
        }
    }

    private Vector3 startPosition = new Vector3(0, 0, 0);

    public FitnessScore(GameObject gameObject)
    {
        this.gameObject = gameObject;
        startPosition = gameObject.transform.position;
    }
}
