﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(FitnessScoring))]
public class Epoch : MonoBehaviour {
    public float epochLength = 10f;
    public EpochSpawn spawn;

    [SerializeField]
    private GameObject best;
    [SerializeField]
    private float bestScore;

	// Use this for initialization
	void Start () {
        StartEpoch(new List<GameObject>());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void StartEpoch(IList<GameObject> parents)
    {
        var fitnessScoring = GetComponent<FitnessScoring>();
        if (parents.Count == 0)
        {
            spawn.SpawnPopulation();
        } else
        {
            spawn.SpawnPopulation(parents);
        }

        foreach (var creature in GameObject.FindGameObjectsWithTag("Creature").Where(go => go.activeSelf))
        {
            fitnessScoring.AddContestant(creature);
        }
        StartCoroutine(EpochTimer());
    }

    private IEnumerator EpochTimer()
    {
        yield return new WaitForSeconds(epochLength);
        EndEpoch();
    }

    private void EndEpoch()
    {
        var fitnessScoring = GetComponent<FitnessScoring>();
        var winners = fitnessScoring.fitnessScores.OrderBy(fs => fs.score).Reverse().Take(2);//.Select(fs => fs.gameObject).ToList();
        var bestThisRound = winners.FirstOrDefault();
        var newParents = new List<GameObject>();
        if (bestThisRound != null)
        {
            if (best == null || bestThisRound.score > bestScore)
            {
                best = bestThisRound.gameObject;
                bestScore = bestThisRound.score;
                foreach (var segment in best.GetComponentsInChildren<BodySegment>())
                {
                    segment.GetComponent<Renderer>().material.color = new Color(0, 1, 0);
                }
            }
        }
        newParents.Add(spawn.SpawnClone(best));
        foreach (var winner in winners)
        {
            newParents.Add(spawn.SpawnClone(winner.gameObject));
        }
        foreach (var creature in GameObject.FindGameObjectsWithTag("Creature").Except(newParents))
        {
            if (creature != best)
            {
                Destroy(creature);
                creature.SetActive(false);
            }
        }
        fitnessScoring.Reset();
        Resources.UnloadUnusedAssets();
        StartEpoch(newParents);
    }
}
