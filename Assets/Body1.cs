﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Creature.Genetics.Body {
	public struct BodySegment {
		public readonly System.Guid ID;
		public readonly System.Guid ConnectedSegmentID;
		//public readonly int ConnectionDepth;
		public readonly double Radius;
		public readonly double Length;
		
		public BodySegment(double radius, double length) {
			ID = System.Guid.NewGuid ();
			ConnectedSegmentID = System.Guid.Empty;
			//ConnectionDepth = 0;
			Radius = radius;
			Length = length;
		}
		
		public BodySegment(BodySegment connectedSegment, double radius, double length) {
			ID = System.Guid.NewGuid();
			ConnectedSegmentID = connectedSegment.ID;
			//ConnectionDepth = connectedSegment.ConnectionDepth + 1;
			Radius = radius;
			Length = length;
		}
		
		public BodySegment(System.Guid id, System.Guid connectedSegmentID, double radius, double length) {
			ID = id;
			ConnectedSegmentID = connectedSegmentID;
			//ConnectionDepth = connectionDepth;
			Radius = radius;
			Length = length;
		}
	}
	
	public class BodyGenome {
		public IList<BodySegment> Segments;
		
		public BodyGenome() {
			Segments = new List<BodySegment> ();
		}
		
		public BodySegment GetSegmentByID(System.Guid id) {
			return Segments.Where (segment => segment.ID == id).First ();
		}
		
		public BodyGenome Clone() {
			var clone = new BodyGenome ();
			clone.Segments = new List<BodySegment> (Segments);
			return clone;
		}
		
		public IList<BodySegment> DescendantsOf(BodySegment segment) {
			var descendants = new List<BodySegment> ();
			descendants.Add (segment);
			foreach (var child in ChildrenOf(segment)) {
				descendants.AddRange(DescendantsOf(child));
			}
			return descendants;
		}
		
		public IList<BodySegment> ChildrenOf(BodySegment segment) {
			return Segments.Where(s => s.ConnectedSegmentID == segment.ID).ToList();
		}
	}

	public static class BodyUtility {


		public static void SetJointPosition(GameObject segment, Vector3 orientation) {
			var joint = segment.GetComponent<ConfigurableJoint> ();
			var collider = joint.connectedBody.GetComponent<Collider> ();
			RaycastHit hit;
			collider.Raycast (new Ray (segment.transform.position + orientation * 100, -orientation), hit, 200f);
			joint.connectedAnchor = segment.transform.InverseTransformPoint (hit.point);
			segment.transform.localRotation = Quaternion.FromToRotation (segment.transform.up, orientation);
			segment.transform.localPosition = joint.connectedAnchor + orientation;
		}
	}

	public class BodyMutation {
		public double ReparentProbability = 0.1;
		public double SproutProbability = 0.1;
		public double PruneProbability = 0.1;
		public double RadiusDeltaMax = 0.05;
		public double LengthDeltaMax = 0.2;
		public double MinLength = 1.5;
		public double MaxLength = 4;
		public double MinRadius = 0.4;
		public double MaxRadius = 0.6;

		public void Mutate(GameObject body) {
			foreach (var transform in Enumerable.Range(0, body.transform.childCount).Select( i => body.transform.GetChild(i))) {
				var segment = transform.gameObject;

				//var RadiusDelta =;
				//transform.localScale = transform.localScale + new Vector3(Random.Range((float)Radiu

				MutateShape(segment);
				MutateAttachmentPosition(segment);

			}
		}

		private void MutateShape(GameObject segment) {
			var radiusDelta = Random.Range((float)-RadiusDeltaMax, (float)RadiusDeltaMax);
			var lengthDelta = Random.Range((float)-LengthDeltaMax, (float)LengthDeltaMax);
			var radius = Mathf.Clamp (segment.transform.localScale.x + radiusDelta, MinRadius, MaxRadius);
			var length = Mathf.Clamp (segment.transform.localScale.y + lengthDelta, MinLength, MaxLength);
			segment.transform.localScale = new Vector3 (radius, length, radius);
		}

		private void MutateAttachmentPosition(GameObject segment) {
			BodyUtility.SetJointPosition (segment, Random.onUnitSphere);
		}

		public BodyGenome Mutate(BodyGenome genome) {
			var newGenome = genome.Clone ();
			newGenome.Segments = genome.Segments.Select (segment => Mutate (genome, segment)).ToList();
			
			var toAdd = new List<BodySegment> ();
			foreach (var segment in newGenome.Segments) {
				if (Random.Range (0f, 1f) < SproutProbability) {
					toAdd.Add(new BodySegment(segment, .5, 2));
				}
			}
			foreach (var segment in toAdd) {
				newGenome.Segments.Add(segment);
			}
			
			var toPrune = new List<BodySegment> ();
			foreach (var segment in newGenome.Segments) {
				if (Random.Range (0f, 1f) < PruneProbability) {
					if (genome.ChildrenOf(segment).Count == 0) {
						toPrune.Add(segment);
					}
				}
			}
			foreach (var segment in toPrune) {
				newGenome.Segments.Remove(segment);
			}
			return newGenome;
		}
		
		public BodySegment Mutate(BodyGenome genome, BodySegment segment) {
			var connectedSegmentID = segment.ConnectedSegmentID;
			
			if (Random.Range (0f, 1f) < ReparentProbability) {
				connectedSegmentID = GetRandomConnectionID(genome, genome.DescendantsOf(segment).Select(s => s.ID).ToList());
			}
			var radius = Mathf.Clamp((float)segment.Radius + Random.Range ((float)-RadiusDeltaMax, (float)RadiusDeltaMax), (float)MinRadius, (float)MaxRadius);
			var length = Mathf.Clamp((float)segment.Length + Random.Range ((float)-LengthDeltaMax, (float)LengthDeltaMax), (float)MinLength, (float)MaxLength);
			return new BodySegment (segment.ID, connectedSegmentID, radius, length);
		}
		
		private System.Guid GetRandomConnectionID(BodyGenome genome, IList<System.Guid> invalidAttachments) {
			var validAttachments = genome.Segments.Where (segment => !invalidAttachments.Contains (segment.ID));
			return validAttachments.OrderBy(x => System.Guid.NewGuid()).FirstOrDefault().ID;
		}
	}
}
*/