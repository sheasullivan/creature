﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class FitnessScoring : MonoBehaviour
{
    public IList<FitnessScore> fitnessScores = new List<FitnessScore>();

    public void AddContestant(GameObject contestant)
    {
        fitnessScores.Add(new FitnessScore(contestant));
    }

    public void Reset()
    {
        fitnessScores = new List<FitnessScore>();
    }
}
