﻿using UnityEngine;
using System.Collections.Generic;
using Creature.NeuralNetwork;

public static class MyUtilities {
	public static bool RollProbability(double percentLikelihood) {
		return Random.Range (0f, 1f) < percentLikelihood;
	}

    public static float GaussianRandom(float standardDeviation, float mean)
    {
        float u, v, S;

        do
        {
            u = 2.0f * Random.value - 1.0f;
            v = 2.0f * Random.value - 1.0f;
            S = u * u + v * v;
        }
        while (S >= 1.0);

        float fac = Mathf.Sqrt(-2.0f * Mathf.Log(S) / S);
        return u * fac * standardDeviation + mean;
    }
}
