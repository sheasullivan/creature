﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Creature.Genetics {
	public class BodyMutation {
		public double ReparentProbability = 0.1;
		public double SproutProbability = 0.01;
		public double PruneProbability = 0.01;
        public double MutateAttachmentProbability = 0.02;
		public double RadiusDeltaMax = 0.05;
		public double LengthDeltaMax = 0.2;
		public double MinLength = 1.5;
		public double MaxLength = 4;
		public double MinRadius = 0.4;
		public double MaxRadius = 0.6;

		public void Mutate(GameObject creature) {
			foreach (var segment in creature.GetComponent<CreatureController>().bodySegments) {
				//var RadiusDelta =;
				//transform.localScale = transform.localScale + new Vector3(Random.Range((float)Radiu

				MutateShape(segment);

                if (MyUtilities.RollProbability(MutateAttachmentProbability))
                {
                    MutateAttachmentPosition(segment);
                }

				if (MyUtilities.RollProbability(SproutProbability)) {
					segment.SproutNewSegment();
				}
				//if (MyUtilities.RollProbability(PruneProbability)) {
				//	segment.Prune();
				//}
			}
		}

		private void MutateShape(BodySegment segment) {
            /*var radiusDelta = Random.Range((float)-RadiusDeltaMax, (float)RadiusDeltaMax);
			var lengthDelta = Random.Range((float)-LengthDeltaMax, (float)LengthDeltaMax);
			var radius = Mathf.Clamp ((float)segment.transform.localScale.x + radiusDelta, (float)MinRadius, (float)MaxRadius);
			var length = Mathf.Clamp ((float)segment.transform.localScale.y + lengthDelta, (float)MinLength, (float)MaxLength);
			segment.transform.localScale = new Vector3 (radius, length, radius);*/
            var scale = Mathf.Clamp(segment.transform.localScale.x * MyUtilities.GaussianRandom(0.1f, 1), 0.8f, 1.5f);
			segment.transform.localScale = new Vector3 (scale, scale, scale);
            segment.GetComponent<Rigidbody>().mass = (float)segment.density * Mathf.Pow(segment.transform.localScale.x, 3);
		}

		private void MutateAttachmentPosition(BodySegment segment) {
            if (segment.hasJoint)
            {
                var newAttachment = segment.GetConnectionPositionOnUnitSphere();
                newAttachment += new Vector3(MyUtilities.GaussianRandom(0.1f, 0), MyUtilities.GaussianRandom(0.1f, 0), MyUtilities.GaussianRandom(0.1f, 0));// Random.onUnitSphere * 0.2f;
                segment.SetConnectionPosition(newAttachment.normalized);
            }
		}
	}
}
