﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	public int HPBonus = 10;

	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter(Collider other) {
		var eater = other.gameObject.GetComponent<BodySegment>().creature.GetComponent<Health> ();
		if (eater != null) {
			eater.Eat(this);
			StartCoroutine(Eaten());
		}
	}

	private IEnumerator Eaten() {
		Deactivate ();
		yield return new WaitForSeconds(90f);
		Activate ();
	}

	public void Deactivate() {
		GetComponent<Renderer> ().enabled = false;
		GetComponent<Collider> ().enabled = false;
		//transform.FindChild("Scent").GetComponent<Scent>().Deactivate();
	}

	public void Activate() {
		GetComponent<Renderer> ().enabled = true;
		GetComponent<Collider> ().enabled = true;
		//transform.FindChild("Scent").GetComponent<Scent>().Activate();
	}
}
