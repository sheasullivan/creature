﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Creature.NeuralNetwork;

namespace Creature.Genetics {
	public class BrainMutation {
		//public double ConnectionAddProbability = 0.1;
		//public double ConnectionRemoveProbability = 0.1;
		//public double SproutProbability = 0.1;
		//public double PruneProbability = 0.1;
		public double MutateConnectionWeightProbability = 0.2;
        public float mutateStrength = 1f;

		public void Mutate(GameObject creature) {
			var nervousSystem = creature.GetComponent<NervousSystem> ();
			var brain = nervousSystem.Brain;
            
            foreach (var neuron in brain.GetCells().OfType<INeuron>())
            {
                MutateNeuron(neuron, brain);
            }

			/*foreach (var neuron in neuronsToRemove) {
				brain.cells.Remove (neuron);
				// TODO: remove dendrites connected to this neuron
				// TODO: avoid removing motor neurons
			}*/

			//if (MyUtilities.RollProbability (SproutProbability)) {
			//	brain.cells.Add (new Neuron (nervousSystem.ActivationFunction));
			//}
		}

		private bool MutateNeuron(INeuron neuron, Brain brain) {
			var dendritesToRemove = new List<Dendrite> ();
			foreach (var dendrite in neuron.dendrites) {
				if (MyUtilities.RollProbability (MutateConnectionWeightProbability)) {
                    dendrite.Strength += MyUtilities.GaussianRandom(mutateStrength, 0);// Random.Range (-mutateStrength, mutateStrength);
                    //dendrite.Strength = Mathf.Clamp((float)dendrite.Strength, -1, 1);
				}
				
				//if (MyUtilities.RollProbability (ConnectionRemoveProbability)) {
				//	dendritesToRemove.Add (dendrite);
				//}
			}
			//foreach (var dendrite in dendritesToRemove) {
			//	neuron.dendrites.Remove (dendrite);
			//}
			
			//if (MyUtilities.RollProbability (ConnectionAddProbability)) {
			//	neuron.dendrites.Add (new Dendrite (brain.GetRandomCell (), Random.Range (-1f, 1f)));
			//}
			
			//if (MyUtilities.RollProbability (PruneProbability)) {
			//	return true;
			//}

			return false;
		}
	}
}
