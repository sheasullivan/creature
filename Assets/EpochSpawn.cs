﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Creature.NeuralNetwork;
using Creature.Genetics;

public class EpochSpawn : MonoBehaviour {
	public GameObject BaseCreature;
	//public GameObject Segment;
	//public Population Population;
	public double Radius = 20;
	public int population = 1;
    public Mutation Mutation = new Mutation();

    // Use this for initialization
    void Start () {
		//var creatureFactory = new CreatureFactory (transform, BaseCreature, Segment);
		//Population = new Population (creatureFactory);
	}
	
    public void SpawnPopulation(IList<GameObject> parents)
    {
        foreach (var parent in parents)
        {
            foreach (int i in Enumerable.Range(0, population / parents.Count))
            {
                SpawnOffspring(parent);
            }
        }
    }

    public GameObject SpawnClone(GameObject parent)
    {
        var c = Random.insideUnitCircle;
        var position = transform.position + new Vector3(c.x, 0, c.y) * (float)Radius;
        var clone = Instantiate(parent, position, Quaternion.identity) as GameObject;
        clone.name = "Creature " + System.Guid.NewGuid();

        var nervousSystem = parent.GetComponent<NervousSystem>();
        var cloneNervousSystem = clone.GetComponent<NervousSystem>();
        cloneNervousSystem.Brain = nervousSystem.Brain.Clone();
        cloneNervousSystem.FindNeurons();

        foreach (var segment in clone.GetComponent<CreatureController>().bodySegments)
        {
            var r = segment.GetComponent<Renderer>();
            r.material.color = new Color(0, 0, 1);
        }

        return clone;
    }

    private void SpawnOffspring(GameObject parent)
    {
        var offspring = SpawnClone(parent);
        Mutation.Mutate(offspring);
        offspring.GetComponent<NervousSystem>().FindNeurons();

        foreach (var segment in offspring.GetComponent<CreatureController>().bodySegments)
        {
            var r = segment.GetComponent<Renderer>();
            r.material.color = new Color(1, 1, 1);
        }
    }

    public void SpawnPopulation()
    {
        SpawnInitialPopulation();
    }

	private void SpawnInitialPopulation() {
		/*var genome = new Genome ();
		genome.BodyGenome.Segments.Add (new BodySegment (Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [0], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [0], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [1], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));
		genome.BodyGenome.Segments.Add (new BodySegment (genome.BodyGenome.Segments [2], Random.Range(.1f, 1f), Random.Range(.5f, 4f)));

		var mutation = new Mutation ();
		for (int x = 0; x < 10; x++) {
			var mutatedGenome = mutation.Mutate(genome);
			Population.Birth(mutatedGenome);
		}*/
		for (int i = 0; i < population; i++) {
			var c = Random.insideUnitCircle;
			var position = transform.position + new Vector3(c.x, 0, c.y) * (float)Radius;
			var creature = Instantiate(BaseCreature);
            creature.name = "Creature " + System.Guid.NewGuid();
            creature.transform.position = position;

            creature.GetComponent<Reproduction>().enabled = false;
            creature.GetComponent<Health>().enabled = false;

            creature.GetComponent<NervousSystem>().Setup();
            foreach (var segment in creature.GetComponentsInChildren<BodySegment>())
            {
                segment.Setup();
            }
            foreach (var nose in creature.GetComponentsInChildren<Nose>()) {
				nose.ConnectNervousSystem();
			}
			//var s0 = creature;
   //         s0.GetComponent<BodySegment>().creature = creature;
   //         var s1 = s0.GetComponent<BodySegment>().SproutNewSegment();
			//var s2 = s1.GetComponent<BodySegment>().SproutNewSegment();
			//var s3 = s2.GetComponent<BodySegment>().SproutNewSegment();
			//var s4 = s0.GetComponent<BodySegment>().SproutNewSegment();
			//s1.GetComponent<BodySegment>().SproutNewSegment();
			//s2.GetComponent<BodySegment>().SproutNewSegment();
		}

	}
}

