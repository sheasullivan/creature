﻿/*using UnityEngine;
using System.Collections;

public class Locomotion : MonoBehaviour {
	private ConfigurableJoint[] joints;
	private float timerStart;
	private bool beat = false;

	// Use this for initialization
	void Start () {
		joints = GetComponentsInChildren<ConfigurableJoint> ();

		foreach (var joint in joints) {
			joint.rotationDriveMode = RotationDriveMode.Slerp;

			var drive = new JointDrive ();
			drive.mode = JointDriveMode.Velocity;
			drive.maximumForce = 50000f;
			drive.positionSpring = 500f;
			drive.positionDamper = 50f;
			joint.slerpDrive = drive;
		}

		timerStart = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - timerStart > 1) {
			timerStart = Time.time;
			beat = !beat;
			//var velocity = beat? new Vector3(10, 0, 0) : new Vector3(-10, 0, 0);
			var velocity = Random.insideUnitSphere * 10;
			foreach (var joint in joints) {
				joint.targetAngularVelocity = velocity;
			}
		}
	}

	public void RotateJointX(ConfigurableJoint joint, double velocity) {
		var target = joint.targetAngularVelocity;
		target.Set ((float)velocity, target.y, target.z);
	}
}
*/