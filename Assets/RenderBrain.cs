﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Creature.NeuralNetwork;
using UnityStandardAssets.Cameras;

public class RenderBrain : MonoBehaviour {
	public enum SectionEnum {Sensor, NeuronA, NeuronB, NeuronC, Motor}

	public GameObject Panel;
	public GameObject Creature;
	public SectionEnum Section;

	private Brain m_brain;
	//private Dictionary<ICell, GameObject> m_panels;

	// Use this for initialization
	void Start () {
		//m_panels = new Dictionary<ICell, GameObject> ();
	}
	
	void FixedUpdate () {
		Creature = GameObject.FindGameObjectWithTag ("Creature");
		/*foreach (var kvp in m_panels) {
			var img = kvp.Value.GetComponent<Image>();
			img.color = new Color((float)kvp.Key.LastOutput, 0, 0);
		}*/
		foreach (var image in GetComponentsInChildren<Image>()) {
			Destroy(image.gameObject);
		}
		//foreach (var kvp in m_panels) {
		//	Destroy(kvp.Value);
		//}
		//m_panels.Clear ();

		if (Creature == null || Creature.GetComponent<NervousSystem>() == null) {
			return;
		}
        //Camera.main.transform.parent.parent = Creature.transform;
		//Camera.main.GetComponent<RTS_Cam.RTS_Camera> ().SetTarget(Creature.transform.GetChild (0).transform);
		//Camera.main.transform.localPosition = new Vector3 (0, 0, -20);
		m_brain = Creature.GetComponent<NervousSystem> ().Brain;
		if (m_brain == null) {
			return;
		}

		IList<ICell> cells;
		switch (Section) {
		case SectionEnum.Sensor:
			cells = m_brain.GetCells(Creature.GetComponent<NervousSystem>().sensorLayer);
			break;
		case SectionEnum.Motor:
            cells = m_brain.GetCells(Creature.GetComponent<NervousSystem>().motorLayer);
            break;
		case SectionEnum.NeuronA:
            cells = m_brain.GetCells(Creature.GetComponent<NervousSystem>().hiddenLayerA);
            break;
		case SectionEnum.NeuronB:
            cells = m_brain.GetCells(Creature.GetComponent<NervousSystem>().hiddenLayerB);
            break;
		case SectionEnum.NeuronC:
            cells = m_brain.GetCells(Creature.GetComponent<NervousSystem>().hiddenLayerC);
            break;
		default:
            cells = m_brain.GetCells();
			break;
		}

		foreach (var cell in cells) {
			//try {
				var p = Instantiate (Panel);
				p.transform.SetParent (transform);
				var img = p.GetComponent<Image> ();
				img.color = new Color((float)cell.lastOutput, 0, 0);
				//m_panels.Add(cell, p);
			//} catch {
			//}
		}
	}
}
