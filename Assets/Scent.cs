﻿using UnityEngine;
using System.Collections;

public enum Smell {
	Food,
	Poison
}

public class Scent : MonoBehaviour {
	public Smell smell;
	public double intensity = 1;

	// Use this for initialization
	void Start () {
	
	}

	public void Deactivate() {
		//GetComponent<Rigidbody> ().enabled = false;
	}
	
	public void Activate() {
		//GetComponent<Rigidbody> ().enabled = true;
	}
}
