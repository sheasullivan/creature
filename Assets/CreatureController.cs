﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatureController : MonoBehaviour {
	public NervousSystem nervousSystem {
		get { return GetComponent<NervousSystem> (); }
	}

	public IList<BodySegment> bodySegments {
		get { return new List<BodySegment>(GetComponentsInChildren<BodySegment>()); }
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
