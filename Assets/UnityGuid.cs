﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class UnityGuid {
	public readonly System.Guid guid;
	public string guidString;

	public UnityGuid(System.Guid guid) {
		this.guid = guid;
		guidString = guid.ToString ();
	}

	public static UnityGuid NewGuid() {
		return new UnityGuid (System.Guid.NewGuid ());
	}
}
