﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.Assertions;
using Creature.NeuralNetwork;
using System.Collections.Generic;

public class BodySegment : MonoBehaviour {
	public GameObject segmentPrefab;
	public double motorAmplification = 1;
    public double density = 100;
    public int minSegments = 2;
	public int maxSegments = 10;
	public GameObject creature;

	//public IList<SensorCell> sensorCells { get; private set; }
	//public IList<MotorCell> motorCells { get; private set; }
	public string motorNeuronXplusID;
	//public string motorNeuronXminusID;
	public string motorNeuronZplusID;
	//public string motorNeuronZminusID;
	public string sensorAngleXID;
	public string sensorAngleZID;
	public string sensorVelocityXID;
    public string sensorVelocityYID;
    public string sensorVelocityZID;
    public string sensorOrientationXID;
    public string sensorOrientationYID;
    public string sensorOrientationZID;

    private INeuron m_motorNeuronXplus;
	//private INeuron m_motorNeuronXminus;
	private INeuron m_motorNeuronZplus;
	//private INeuron m_motorNeuronZminus;
	private SensorCell m_sensorAngleX;
	private SensorCell m_sensorAngleZ;
	private SensorCell m_sensorVelocityX;
    private SensorCell m_sensorVelocityY;
    private SensorCell m_sensorVelocityZ;
    private SensorCell m_sensorOrientationX;
    private SensorCell m_sensorOrientationY;
    private SensorCell m_sensorOrientationZ;

    //public BodySegment() : base() {
    /*sensorCells = new List<SensorCell> ();
    motorCells = new List<MotorCell> ();
    motorCells.Add (new MotorCell (activ));*/
    //}

    private Vector3 m_jointMovementInput;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Vector3 angularVelocity {
		get {
			var rb = GetComponent<Rigidbody> ();
			if (rb == null) {
				return Vector3.zero;
			}
			return rb.angularVelocity;
		}
	}

    public Vector3 orientation
    {
        get
        {
            var rb = GetComponent<Rigidbody>();
            if (rb == null)
            {
                return Vector3.zero;
            }
            return rb.rotation.eulerAngles;
        }
    }

    public bool hasJoint {
        get
        {
            return GetComponents<ConfigurableJoint>().Length > 0;
        }
    }

    public Vector3 jointAngle {
		get {
			return transform.localEulerAngles;
		}
	}

	public Vector3 jointMovementInput {
		get {
			return m_jointMovementInput;
		}
		set {
			m_jointMovementInput = value;

            try {
                var joint = GetComponent<ConfigurableJoint>();
                if (joint == null)
                {
                    return;
                }

                //var nervousSystem = creature.GetComponent<NervousSystem>();
                //var x = nervousSystem.ActivationFunction.Evaluate(m_jointMovementInput.x);
                //var y = nervousSystem.ActivationFunction.Evaluate(m_jointMovementInput.y);
                //var z = nervousSystem.ActivationFunction.Evaluate(m_jointMovementInput.z);

                //joint.targetRotation = Quaternion.Euler(90, 0, 90);
                //var xdrive = joint.angularXDrive;
                //var zdrive = joint.angularYZDrive;
                //xdrive.positionSpring = m_jointMovementInput.x > 0.5f ? (float)motorAmplification : 0;
                //zdrive.positionSpring = m_jointMovementInput.z > 0.5f ? (float)motorAmplification : 0;
                //joint.angularXDrive = xdrive;
                //joint.angularYZDrive = zdrive;

                //Debug.Log(m_jointMovementInput);
                //Debug.Log(joint);
                //Debug.Log(joint.targetRotation);

                joint.targetRotation = Quaternion.Euler(
                    ((m_jointMovementInput.x * 90) - 45) * (float)motorAmplification,
                    ((m_jointMovementInput.y * 90) - 45) * (float)motorAmplification,
                    ((m_jointMovementInput.z * 90)) * (float)motorAmplification);
            }
            catch (MissingComponentException)
            {
                return;
            }
        }
	}

    public void Setup()
    {
        var controller = creature.GetComponent<CreatureController>();
        controller.bodySegments.Add(this);
        ConnectNervousSystem();
        FindNeurons();
    }

    public void RunSensors() {
		var nervousSystem = creature.GetComponent<NervousSystem> ();
		if (nervousSystem == null || nervousSystem.Brain == null) {
			return;
		}
		if (m_sensorAngleX != null) {
			m_sensorAngleX.input = jointAngle.x;
		}
		if (m_sensorAngleZ != null) {
			m_sensorAngleZ.input = jointAngle.z;
		}
		if (m_sensorVelocityX != null) {
			m_sensorVelocityX.input = angularVelocity.x;
		}
        if (m_sensorVelocityY != null) {
            m_sensorVelocityY.input = angularVelocity.y;
        }
        if (m_sensorVelocityZ != null) {
			m_sensorVelocityZ.input = angularVelocity.z;
		}
        if (m_sensorOrientationX != null)
        {
            m_sensorOrientationX.input = orientation.x;
        }
        if (m_sensorOrientationY != null)
        {
            m_sensorOrientationY.input = orientation.y;
        }
        if (m_sensorOrientationZ != null)
        {
            m_sensorOrientationZ.input = orientation.z;
        }
    }

	public void RunMotors() {
		//var nervousSystem = creature.GetComponent<NervousSystem> ();
		//if (nervousSystem == null || nervousSystem.Brain == null) {
		//	return;
		//}

		float x = 0;
		float y = 0;
		float z = 0;
		if (m_motorNeuronXplus != null) {
			x += (float)m_motorNeuronXplus.lastOutput;
		}
		//if (m_motorNeuronXminus != null) {
		//	x -= (float)m_motorNeuronXminus.lastOutput;
		//}
		if (m_motorNeuronZplus != null) {
			z += (float)m_motorNeuronZplus.lastOutput;
		}
        //if (m_motorNeuronZminus != null) {
        //	z -= (float)m_motorNeuronZminus.lastOutput;
        //}

        jointMovementInput = new Vector3 (x, y, z);
        //jointMovementInput = new Vector3(0, 0, 0);
    }


	public GameObject SproutNewSegment() {
		var controller = creature.GetComponent<CreatureController> ();
		if (controller.bodySegments.Count >= maxSegments) {
			return null;
		}
		var newGameObject = Instantiate (segmentPrefab);
		newGameObject.name = "Segment " + System.Guid.NewGuid ();
		var newSegment = newGameObject.GetComponent<BodySegment> ();
		newSegment.creature = creature;

		newGameObject.GetComponent<Rigidbody> ().mass = GetComponent<Rigidbody> ().mass;
		newGameObject.GetComponent<ConfigurableJoint> ().connectedBody = GetComponent<Rigidbody> ();
        newGameObject.transform.SetParent(creature.transform);

		newSegment.SetConnectionPosition (transform.TransformDirection(Vector3.up));

        newSegment.Setup();

		return newGameObject;
	}

	public void Prune() {
        // Don't prune root segment
        try {
            if (GetComponent<ConfigurableJoint>() == null) {
                return;
            }
        } catch (MissingComponentException e)
        {
            return;
        }

		var controller = creature.GetComponent<CreatureController> ();
		if (controller.bodySegments.Count <= minSegments) {
			return;
		}

		// Cut out this segment, attaching joints to its parent
		foreach (var segment in controller.bodySegments) {
			if (segment.GetComponent<ConfigurableJoint>().connectedBody == GetComponent<Rigidbody>()) {
				segment.GetComponent<ConfigurableJoint>().connectedBody = GetComponent<ConfigurableJoint>().connectedBody;
			}
		}

		controller.bodySegments.Remove (this);
		Object.Destroy (gameObject);
	}

    public Vector3 GetConnectionPositionOnUnitSphere()
    {
        var joint = GetComponent<ConfigurableJoint>();
        return joint.connectedAnchor.normalized;
    }

	public void SetConnectionPosition(Vector3 orientation) {
		var joint = GetComponent<ConfigurableJoint> ();
		if (joint == null || joint.connectedBody == null) {
			return;
		}
		var collider = joint.connectedBody.GetComponent<Collider> ();
		var unitOrientation = Vector3.ClampMagnitude (orientation, 1);

		// Map orientation to position on connected surface
		RaycastHit hit;
		var didHit = collider.Raycast (new Ray (joint.connectedBody.transform.position + unitOrientation * 100, -unitOrientation), out hit, 200f);
		Assert.IsTrue (didHit);
		joint.connectedAnchor = joint.connectedBody.transform.InverseTransformPoint (hit.point);
		transform.rotation = Quaternion.FromToRotation (transform.up, unitOrientation);
		transform.position = joint.connectedBody.transform.TransformPoint(joint.connectedAnchor) + unitOrientation;
	}

	public void FindNeurons() {
		var nervousSystem = creature.GetComponent<NervousSystem> ();
		if (nervousSystem == null || nervousSystem.Brain == null) {
			return;
		}
		m_motorNeuronXplus = System.String.IsNullOrEmpty(motorNeuronXplusID) ? null : nervousSystem.Brain.GetCellByGuid(motorNeuronXplusID) as INeuron;
		//m_motorNeuronXminus = System.String.IsNullOrEmpty(motorNeuronXminusID) ? null : nervousSystem.Brain.GetCellByGuid(motorNeuronXminusID) as INeuron;
		m_motorNeuronZplus = System.String.IsNullOrEmpty(motorNeuronZplusID) ? null : nervousSystem.Brain.GetCellByGuid(motorNeuronZplusID) as INeuron;
		//m_motorNeuronZminus = System.String.IsNullOrEmpty(motorNeuronZminusID) ? null : nervousSystem.Brain.GetCellByGuid(motorNeuronZminusID) as INeuron;
		m_sensorAngleX = System.String.IsNullOrEmpty(sensorAngleXID) ? null : nervousSystem.Brain.GetCellByGuid(sensorAngleXID) as SensorCell;
		m_sensorAngleZ = System.String.IsNullOrEmpty(sensorAngleZID) ? null : nervousSystem.Brain.GetCellByGuid(sensorAngleZID) as SensorCell;
		m_sensorVelocityX = System.String.IsNullOrEmpty(sensorVelocityXID) ? null : nervousSystem.Brain.GetCellByGuid(sensorVelocityXID) as SensorCell;
        m_sensorVelocityY = System.String.IsNullOrEmpty(sensorVelocityYID) ? null : nervousSystem.Brain.GetCellByGuid(sensorVelocityYID) as SensorCell;
        m_sensorVelocityZ = System.String.IsNullOrEmpty(sensorVelocityZID) ? null : nervousSystem.Brain.GetCellByGuid(sensorVelocityZID) as SensorCell;
        m_sensorOrientationX = System.String.IsNullOrEmpty(sensorOrientationXID) ? null : nervousSystem.Brain.GetCellByGuid(sensorOrientationXID) as SensorCell;
        m_sensorOrientationY = System.String.IsNullOrEmpty(sensorOrientationYID) ? null : nervousSystem.Brain.GetCellByGuid(sensorOrientationYID) as SensorCell;
        m_sensorOrientationZ = System.String.IsNullOrEmpty(sensorOrientationZID) ? null : nervousSystem.Brain.GetCellByGuid(sensorOrientationZID) as SensorCell;
    }

	private void ConnectNervousSystem ()
	{
		var nervousSystem = creature.GetComponent<NervousSystem> ();
		if (nervousSystem == null || nervousSystem.Brain == null) {
			return;
		}
		var activationFunction = nervousSystem.ActivationFunction;

		var mnxp = new Neuron (activationFunction);
		motorNeuronXplusID = mnxp.guid;
		//var mnxm = new Neuron (activationFunction);
		//motorNeuronXminusID = mnxm.guid;
		var mnzp = new Neuron (activationFunction);
		motorNeuronZplusID = mnzp.guid;
		//var mnzm = new Neuron (activationFunction);
		//motorNeuronZminusID = mnzm.guid;
		var sax = new SensorCell (activationFunction);
		sensorAngleXID = sax.guid;
		var saz = new SensorCell (activationFunction);
		sensorAngleZID = saz.guid;
		var svx = new SensorCell (activationFunction);
		sensorVelocityXID = svx.guid;
        var svy = new SensorCell(activationFunction);
        sensorVelocityYID = svy.guid;
        var svz = new SensorCell (activationFunction);
		sensorVelocityZID = svz.guid;
        var sox = new SensorCell(activationFunction);
        sensorOrientationXID = sox.guid;
        var soy = new SensorCell(activationFunction);
        sensorOrientationYID = soy.guid;
        var soz = new SensorCell(activationFunction);
        sensorOrientationZID = soz.guid;

        nervousSystem.Brain.AddNewCell(mnxp, nervousSystem.motorLayer);
		//nervousSystem.Brain.AddNewCell(mnxm, nervousSystem.motorLayer);
		nervousSystem.Brain.AddNewCell(mnzp, nervousSystem.motorLayer);
		//nervousSystem.Brain.AddNewCell(mnzm, nervousSystem.motorLayer);
		nervousSystem.Brain.AddNewCell(sax, nervousSystem.sensorLayer);
		nervousSystem.Brain.AddNewCell(saz, nervousSystem.sensorLayer);
		nervousSystem.Brain.AddNewCell(svx, nervousSystem.sensorLayer);
        nervousSystem.Brain.AddNewCell(svy, nervousSystem.sensorLayer);
        nervousSystem.Brain.AddNewCell(svz, nervousSystem.sensorLayer);
        nervousSystem.Brain.AddNewCell(sox, nervousSystem.sensorLayer);
        nervousSystem.Brain.AddNewCell(soy, nervousSystem.sensorLayer);
        nervousSystem.Brain.AddNewCell(soz, nervousSystem.sensorLayer);
    }
}
