﻿using UnityEngine;
using System.Collections;

public class FoodSpawn : MonoBehaviour {
	public GameObject Food;
	public int Count = 100;
	public int Radius = 100;

	// Use this for initialization
	void Start () {
		StartCoroutine (Tick ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}	

	private IEnumerator Tick() {
		//while (true) {
			for (int i = 0; i < Count; i++) {
				var rand = Random.insideUnitCircle;
				var posOnPlane = transform.position + new Vector3(rand.x, 0, rand.y) * Radius;
				var posOnTerrain = new Vector3(posOnPlane.x, Terrain.activeTerrain.SampleHeight(posOnPlane), posOnPlane.z);
				Instantiate(Food, posOnTerrain + Vector3.up, Quaternion.identity);
			}
			yield return new WaitForSeconds(1000f);
		//}
	}
}
