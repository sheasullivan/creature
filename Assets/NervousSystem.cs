﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Creature.NeuralNetwork;
using System;

public class NervousSystem : MonoBehaviour {
	public IActivationFunction ActivationFunction = new SigmoidFunction();
	public Brain Brain {
		get {
			return m_brain;
		}
		set {
			m_brain = value;
		}
	}
	public string pulseSensorID;
    public int sensorLayer;
    public int hiddenLayerA;
    public int hiddenLayerB;
    public int hiddenLayerC;
    public int motorLayer;

	private Brain m_brain;
	private SensorCell m_pulseSensor;

	void FixedUpdate () {
		if (m_pulseSensor != null) {
			m_pulseSensor.input = Mathf.Sin (Time.time * 10);
		}
		foreach (var segment in GetComponent<CreatureController>().bodySegments) {
			segment.RunSensors();
		}
		//foreach (var nose in transform.root.GetComponentsInChildren<Nose>()) {
		//	nose.Sniff();
		//}
		m_brain.Evaluate ();
		foreach (var segment in GetComponent<CreatureController>().bodySegments) {
			segment.RunMotors();
		}
	}

	public void Setup() {
		m_brain = new Brain ();
		SetupBrain ();
		//m_brain.Randomize ();
	}

	private void SetupBrain () {
        m_brain.AddLayer();
        sensorLayer = 0;
        m_brain.AddLayer();
        hiddenLayerA = 1;
        m_brain.AddLayer();
        hiddenLayerB = 2;
        m_brain.AddLayer();
        hiddenLayerC = 3;
        m_brain.AddLayer();
        motorLayer = 4;



        for (int i = 0; i < 18; i++) {
			var neuron = new Neuron(ActivationFunction);
			m_brain.AddNewCell(neuron, hiddenLayerA);
		}
        for (int i = 0; i < 16; i++)
        {
            var neuron = new Neuron(ActivationFunction);
            m_brain.AddNewCell(neuron, hiddenLayerB);
        }
        for (int i = 0; i < 12; i++)
        {
            var neuron = new Neuron(ActivationFunction);
            m_brain.AddNewCell(neuron, hiddenLayerC);
        }

  //      var pulse = new SensorCell (ActivationFunction);
		//m_brain.AddNewCell (pulse, sensorLayer);
		//pulseSensorID = pulse.guid;
  //      m_pulseSensor = pulse;

		/*var segments = GetComponentsInChildren<BodySegment> ();
		foreach (var segment in segments) {
			foreach (var cell in segment.SensorCells) {
				m_brain.AddNewCell(cell);
			}
			foreach (var cell in segment.MotorCells) {
				m_brain.AddNewCell(cell);
			}
		}*/

		/*var layer3 = new List<Neuron> ();
		foreach (var mc in brain.CellsOfType<MotorCell>()) {
			var neuron = new Neuron(ActivationFunction);
			layer3.Add(neuron);
			brain.Cells.Add(neuron);
			mc.Trigger = neuron;
		}

		var layer2 = new List<Neuron> ();
		for (int i = 0; i < layer3.Count * 2; i++) {
			var neuron = new Neuron(ActivationFunction);
			foreach (var n in layer3) {
				n.Dendrites.Add(new Dendrite(neuron));
			}
			layer2.Add(neuron);
			brain.Cells.Add(neuron);
		}

		var layer1 = new List<Neuron> ();
		for (int i = 0; i < layer2.Count * 2; i++) {
			var neuron = new Neuron(ActivationFunction);
			foreach (var n in layer2) {
				n.Dendrites.Add(new Dendrite(neuron));
			}
			foreach (var sc in brain.CellsOfType<SensorCell>()) {
				neuron.Dendrites.Add(new Dendrite(sc));
			}
			layer1.Add(neuron);
			brain.Cells.Add(neuron);
		}*/
		
		//return brain;
	}

	public void FindNeurons() {
		m_pulseSensor = System.String.IsNullOrEmpty(pulseSensorID) ? null : m_brain.GetCellByGuid (pulseSensorID) as SensorCell;
		foreach (var segment in GetComponent<CreatureController>().bodySegments) {
			segment.FindNeurons();
		}
	}
}
