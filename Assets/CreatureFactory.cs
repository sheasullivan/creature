﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Creature.Genetics;
using Creature.Genetics.Body;

public class CreatureFactory {
	public Transform Spawn;
	public GameObject BaseCreature;
	public GameObject Segment;
	
	public CreatureFactory(Transform spawn, GameObject baseCreature, GameObject segment) {
		Spawn = spawn;
		BaseCreature = baseCreature;
		Segment = segment;
	}
	
	public GameObject Create(Genome genome, Vector3 localPosition) {
		var newCreature = Object.Instantiate (BaseCreature, Spawn.position + localPosition, Quaternion.identity) as GameObject;
		CreateAnatomy (newCreature, genome.BodyGenome);

		//var nervousSystem = newCreature.GetComponent<NervousSystem> ();
		//nervousSystem.Brain = BrainFactory (genome);
		return newCreature;
	}

	private void CreateAnatomy(GameObject creature, BodyGenome bGenome) {
		var instantiatedSegments = new Dictionary<System.Guid, GameObject> ();
		foreach (var segment in bGenome.Segments) {
			AddSegment(creature, bGenome, segment.ID, instantiatedSegments);
		}
	}

	private GameObject AddSegment(GameObject creature, BodyGenome bGenome, System.Guid segmentID, Dictionary<System.Guid, GameObject> instantiatedSegments) {
		if (instantiatedSegments.ContainsKey (segmentID)) {
			return instantiatedSegments [segmentID];
		}
		
		var segment = bGenome.GetSegmentByID (segmentID);
		GameObject connectedGameSegment;
		if (segment.ConnectedSegmentID != System.Guid.Empty) {
			connectedGameSegment = AddSegment (creature, bGenome, segment.ConnectedSegmentID, instantiatedSegments);
		} else {
			connectedGameSegment = creature.transform.GetChild(0).gameObject;
		}
		var gameSegment = Object.Instantiate (Segment);
		gameSegment.transform.SetParent (creature.transform);
		
		var joint = gameSegment.GetComponent<ConfigurableJoint> ();
		var collider = connectedGameSegment.GetComponent<Collider> ();
		var orientation = UnityEngine.Random.onUnitSphere;
		gameSegment.transform.localRotation = Quaternion.LookRotation (orientation);
		gameSegment.transform.localPosition = orientation;
		var attachment = collider.ClosestPointOnBounds (connectedGameSegment.transform.TransformPoint (orientation));
		attachment = connectedGameSegment.transform.InverseTransformPoint(attachment);
		attachment.Scale (new Vector3 (1.01f, 1.01f, 1.01f));
		joint.connectedAnchor = attachment;
		joint.connectedBody = connectedGameSegment.GetComponent<Rigidbody> ();
		
		ScaleSegment (gameSegment, segment.Radius, segment.Length);
		
		instantiatedSegments.Add (segment.ID, gameSegment);
		return gameSegment;
	}
	
	private void ScaleSegment(GameObject segment, double radius, double length) {
		var scale = segment.transform.localScale;
		scale.Scale (new Vector3 ((float)radius * 2, (float)length / 2, (float)radius * 2));
		segment.transform.localScale = scale;
		
		var rb = segment.GetComponent<Rigidbody> ();
		rb.mass = Mathf.Pow(segment.transform.localScale.magnitude, 3f);
	}
}
*/