﻿using UnityEngine;
using System.Collections;

namespace Creature.Genetics {
	public class Mutation {
		public BodyMutation BodyMutation;
		public BrainMutation BrainMutation;

		public Mutation() {
			BodyMutation = new BodyMutation ();
			BrainMutation = new BrainMutation ();
		}

		public void Mutate(GameObject creature) {
			BodyMutation.Mutate (creature);
			BrainMutation.Mutate (creature);
		}
	}
}
