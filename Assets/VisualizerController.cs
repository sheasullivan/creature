﻿using UnityEngine;
using System.Collections;

public class VisualizerController : MonoBehaviour {
    public GameObject visualizer;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            visualizer.SetActive(!visualizer.activeSelf);
        }
    }
}
